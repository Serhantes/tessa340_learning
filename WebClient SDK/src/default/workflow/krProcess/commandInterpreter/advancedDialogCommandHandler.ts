import { ClientCommandHandlerBase } from 'tessa/workflow/krProcess/clientCommandInterpreter';
import { IClientCommandHandlerContext, KrProcessInstance, launchProcess } from 'tessa/workflow/krProcess';
import { tryGetFromInfo, UIContext } from 'tessa/ui';
import { IStorage } from 'tessa/platform/storage';
import { CardTaskCompletionOptionSettings, systemKeyPrefix, CardTaskDialogActionResult } from 'tessa/cards';
import { ICardEditorModel, CardTaskDialogContext, CardTaskDialogOnButtonPressedFunc } from 'tessa/ui/cards';

export class AdvancedDialogCommandHandler extends ClientCommandHandlerBase {

  public async handle(context: IClientCommandHandlerContext) {
    const parameters = context.command.parameters;

    const instanceStorage: IStorage | null = tryGetFromInfo(parameters, 'ProcessInstance', null);
    if (!instanceStorage) {
      return;
    }
    const processInstance = new KrProcessInstance(instanceStorage);

    const coSettingsStorage: IStorage | null = tryGetFromInfo(parameters, 'CompletionOptionSettings', null);
    if (!coSettingsStorage) {
      return;
    }

    const coSettings = new CardTaskCompletionOptionSettings(coSettingsStorage);

    const editor = UIContext.current.cardEditor!;
    editor.info[systemKeyPrefix + 'CardEditorCompletionOptionSettings'] = coSettings;
    editor.info[systemKeyPrefix + 'CardEditorCompletionOptionSettingsOnButtonPressed'] =
      (async (
        dialogCardEditor: ICardEditorModel,
        dialogContext: CardTaskDialogContext,
        buttonName: string,
        _cancel: boolean
      ): Promise<void> => {
        const card = editor.cardModel!.card;
        const dialogCard = dialogCardEditor.cardModel!.card;
        const actionResult = new CardTaskDialogActionResult();
        actionResult.mainCardId = card.id;
        actionResult.pressedButtonName = buttonName;
        actionResult.storeMode = dialogContext.completionOptionSettings.storeMode;
        actionResult.setDialogCard(dialogCard);

        card.info[systemKeyPrefix + 'CardTaskDialogActionResult'] = actionResult.getStorage();

        await launchProcess(processInstance, { cardEditor: editor });

        const response = editor.lastData.storeResponse;

        if (response && response.validationResult.isSuccessful) {
          if (!tryGetFromInfo(response.info, systemKeyPrefix + 'KeepTaskDialog', false)) {
            await dialogCardEditor.close();
          }
        }
      }) as CardTaskDialogOnButtonPressedFunc;
  }

}
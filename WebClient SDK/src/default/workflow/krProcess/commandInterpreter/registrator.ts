import { CreateCardViaDocTypeCommandHandler } from './createCardViaDocTypeCommandHandler';
import { CreateCardViaTemplateCommandHandler } from './createCardViaTemplateCommandHandler';
import { OpenCardClientCommandHandler } from './openCardClientCommandHandler';
import { ShowConfirmationDialogClientCommandHandler } from './showConfirmationDialogClientCommandHandler';
import { AdvancedDialogCommandHandler } from './advancedDialogCommandHandler';
import { ClientCommandInterpreter } from 'tessa/workflow/krProcess/clientCommandInterpreter';

ClientCommandInterpreter.instance.registerHandler('ShowConfirmationDialog', ShowConfirmationDialogClientCommandHandler);
// ClientCommandInterpreter.instance.registerHandler('RefreshAndNotify', RefreshAndNotifyClientCommandHandler); // not used
ClientCommandInterpreter.instance.registerHandler('CreateCardViaTemplate', CreateCardViaTemplateCommandHandler);
ClientCommandInterpreter.instance.registerHandler('CreateCardViaDocType', CreateCardViaDocTypeCommandHandler);
ClientCommandInterpreter.instance.registerHandler('OpenCard', OpenCardClientCommandHandler);
ClientCommandInterpreter.instance.registerHandler('ShowAdvancedDialog', AdvancedDialogCommandHandler);
import { ExtensionContainer, ExtensionStage } from 'tessa/extensions';

import { ClientKrPermissionsGetFileContentExtension } from './clientKrPermissionsGetFileContentExtension';
import { ClientKrPermissionsGetFileVersionsExtension } from './clientKrPermissionsGetFileVersionsExtension';
import { WfCardFileExtension } from './wfCardFileExtension';
ExtensionContainer.instance.registerExtension({extension: ClientKrPermissionsGetFileContentExtension, stage: ExtensionStage.AfterPlatform, singleton: true});
ExtensionContainer.instance.registerExtension({extension: ClientKrPermissionsGetFileVersionsExtension, stage: ExtensionStage.AfterPlatform, singleton: true});
ExtensionContainer.instance.registerExtension({extension: WfCardFileExtension, stage: ExtensionStage.AfterPlatform, singleton: true});

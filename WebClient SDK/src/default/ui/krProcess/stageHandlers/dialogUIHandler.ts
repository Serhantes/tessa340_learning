import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor,
  dialogDescriptor} from 'tessa/workflow/krProcess';
import { plainColumnName } from 'tessa/workflow';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';

export class DialogUIHandler extends KrStageTypeUIHandler {

  private _cardStoreModeId: string = plainColumnName('KrDialogStageTypeSettingsVirtual', 'CardStoreModeID');
  private _openModeId: string = plainColumnName('KrDialogStageTypeSettingsVirtual', 'OpenModeID');
  private _dialogTypeId: string = plainColumnName('KrDialogStageTypeSettingsVirtual', 'DialogTypeID');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [dialogDescriptor];
  }

  public validate(context: IKrStageTypeUIHandlerContext) {
    if (!context.row.tryGet(this._cardStoreModeId)) {
      context.validationResult.add(
        ValidationResult.fromText('$KrStages_Dialog_CardStoreModeNotSpecified', ValidationResultType.Error));
    }

    if (!context.row.tryGet(this._openModeId)) {
      context.validationResult.add(
        ValidationResult.fromText('$KrStages_Dialog_CardOpenModeNotSpecified', ValidationResultType.Error));
    }

    if (!context.row.tryGet(this._dialogTypeId)) {
      context.validationResult.add(
        ValidationResult.fromText('$KrStages_Dialog_DialogTypeIDNotSpecified', ValidationResultType.Error));
    }
  }

}
import { CardUIExtension, ICardUIExtensionContext, ICardModel, IFormViewModel,
  IBlockViewModel, IControlViewModel } from 'tessa/ui/cards';
import { KrToken, KrPermissionVisibilitySettings, ControlType, KrPermissionSectionSettings } from 'tessa/workflow';
import { GridViewModel, GridRowEventArgs } from 'tessa/ui/cards/controls';
import { showNotEmpty, isTopLevelForm } from 'tessa/ui';
import { ValidationResult } from 'tessa/platform/validation';
import { TaskViewModel } from 'tessa/ui/cards/tasks';
import { Visibility, unseal } from 'tessa/platform';
import { LocalizationManager } from 'tessa/localization';
import { DefaultFormTabWithTasksViewModel } from 'tessa/ui/cards/forms';

class VisibilitySettings {

  public blocksForHide: Set<string> = new Set();
  public blocksForShow: Set<string> = new Set();
  public tabsForHide: Set<string> = new Set();
  public tabsForShow: Set<string> = new Set();
  public controlsForHide: Set<string> = new Set();
  public controlsForShow: Set<string> = new Set();

  public blocksPatternsForHide: string[] = [];
  public blocksPatternsForShow: string[] = [];
  public tabsPatternsForHide: string[] = [];
  public tabsPatternsForShow: string[] = [];
  public controlsPatternsForHide: string[] = [];
  public controlsPatternsForShow: string[] = [];

  public fill(visibilitySettings: KrPermissionVisibilitySettings[]) {
    for (let visibilitySetting of visibilitySettings) {
      let patternList: string[];
      let nameList: Set<string>;
      switch (visibilitySetting.controlType) {
        case ControlType.Tab:
          if (visibilitySetting.isHidden) {
            patternList = this.tabsPatternsForHide;
            nameList = this.tabsForHide;
          } else {
            patternList = this.tabsPatternsForShow;
            nameList = this.tabsForShow;
          }
          break;

        case ControlType.Block:
          if (visibilitySetting.isHidden) {
            patternList = this.blocksPatternsForHide;
            nameList = this.blocksForHide;
          } else {
            patternList = this.blocksPatternsForShow;
            nameList = this.blocksForShow;
          }
          break;

        case ControlType.Control:
          if (visibilitySetting.isHidden) {
            patternList = this.controlsPatternsForHide;
            nameList = this.controlsForHide;
          } else {
            patternList = this.controlsPatternsForShow;
            nameList = this.controlsForShow;
          }
          break;

        default:
          continue;
      }

      this.fillName(patternList, nameList, visibilitySetting.alias);
    }
  }

  private fillName(patternList: string[], nameList: Set<string>, alias: string) {
    const length = alias.length;
    const wildStart = alias[0] === '*';
    const wildEnd = alias[length - 1] === '*';

    if (!wildStart && !wildEnd) {
      nameList.add(alias);
    } else {
      const escapedAlias =
        // TODO: escape
        alias.substring(wildStart ? 1 : 0, Math.max(0, length - (wildStart ? 1 : 0) - (wildEnd ? 1 : 0)));
      patternList.push((wildStart ? '' : '^') + escapedAlias + (wildEnd ? '' : '$'));
    }
  }

}

class HideControlsVisitor {

  private _visibilitySettings: VisibilitySettings;

  public hideSections: Set<guid> = new Set();
  public hideFields: Set<guid> = new Set();
  public mandatorySections: Set<guid> = new Set();
  public mandatoryFields: Set<guid> = new Set();

  public visit(cardModel: ICardModel) {
    for (let control of cardModel.controlsBag) {
      this.visitControl(control);
    }

    for (let block of cardModel.blocksBag) {
      this.visitBlock(block);
    }

    for (let form of cardModel.formsBag) {
      this.visitForm(cardModel, form);
    }
  }

  public fill(
    sectionSettings: KrPermissionSectionSettings[],
    visibilitySettings: VisibilitySettings
  ) {
    for (let sectionSetting of sectionSettings) {
      if (sectionSetting.isHidden) {
        this.hideSections.add(sectionSetting.id);
      } else {
        sectionSetting.hiddenFields.forEach(x => this.hideFields.add(x));
      }

      if (sectionSetting.isMandatory) {
        this.mandatorySections.add(sectionSetting.id);
      } else {
        sectionSetting.mandatoryFields.forEach(x => this.mandatoryFields.add(x));
      }
    }

    this._visibilitySettings = visibilitySettings;
  }

  private visitForm(model: ICardModel, form: IFormViewModel) {
    if (form.name) {
      if (this.checkInList(this._visibilitySettings.tabsForHide,
        this._visibilitySettings.tabsPatternsForHide, form.name)
      ) {
        this.hideForm(model, form);
      }
      // Нет возможности добавлять скрытую форму, т.к. она не была сгенерирована
    }
  }

  private visitBlock(block: IBlockViewModel) {
    if (block.name) {
      if (this.checkInList(this._visibilitySettings.blocksForHide,
        this._visibilitySettings.blocksPatternsForHide, block.name)
      ) {
        this.hideBlock(block);
      } else if (block.blockVisibility === Visibility.Collapsed
        && this.checkInList(this._visibilitySettings.blocksForShow,
          this._visibilitySettings.blocksPatternsForShow, block.name)
      ) {
        this.showBlock(block);
      }
    }
  }

  private visitControl(control: IControlViewModel) {
    const sourceInfo = control.cardTypeControl.getSourceInfo();
    if (this.hideSections.has(sourceInfo.sectionId)
      || sourceInfo.columnIds.some(x => this.hideFields.has(x))
    ) {
      this.hideControl(control);
    }

    if (control.name) {
      if (this.checkInList(this._visibilitySettings.controlsForHide,
        this._visibilitySettings.controlsPatternsForHide, control.name)
      ) {
        this.hideControl(control);
      } else if (control.controlVisibility === Visibility.Collapsed
        && this.checkInList(this._visibilitySettings.controlsForShow,
          this._visibilitySettings.controlsPatternsForShow, control.name)
      ) {
        this.showControl(control);
      }
    }

    if (this.mandatorySections.has(sourceInfo.sectionId)
      || sourceInfo.columnIds.some(x => this.mandatoryFields.has(x))
    ) {
      this.makeControlMandatory(control);
    }
  }

  private hideControl(control: IControlViewModel) {
    control.controlVisibility = Visibility.Collapsed;
  }

  private showControl(control: IControlViewModel) {
    control.controlVisibility = Visibility.Visible;
  }

  private hideBlock(block: IBlockViewModel) {
    block.blockVisibility = Visibility.Collapsed;
  }

  private showBlock(block: IBlockViewModel) {
    block.blockVisibility = Visibility.Visible;
  }

  private hideForm(_model: ICardModel, formViewModel: IFormViewModel) {
    if (isTopLevelForm(unseal(formViewModel.cardTypeForm))) {
      formViewModel.visibility = Visibility.Collapsed;
    } else {
      formViewModel.close();
    }
  }

  private makeControlMandatory(controlViewModel: IControlViewModel) {
    controlViewModel.isRequired = true;
    controlViewModel.requiredText = LocalizationManager.instance.format('$KrPermissions_MandatoryControlTemplate',
      controlViewModel.caption);
  }

  private checkInList(
    names: Set<string>,
    patterns: string[],
    checkName: string
  ): boolean {
    if (names.has(checkName)) {
      return true;
    }

    if (patterns) {
      for (let pattern of patterns) {
        if (checkName.match(new RegExp(pattern))) {
          return true;
        }
      }
    }

    return false;
  }

}

export class KrExtendedPermissionsUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    const token = KrToken.tryGet(context.card.info);

    // Если не используется типовое решение
    if (!token || !token.extendedCardSettings) {
      return;
    }

    const hideControlsVisitor = new HideControlsVisitor();

    // Набор визиторов по гридам
    const visitors = new Map<GridViewModel, HideControlsVisitor>();

    // Инициализация видимости контролов по визитору
    const initModel = (initModel: ICardModel, visitor: HideControlsVisitor) => {
      visitor.visit(initModel);
      for (let control of initModel.controlsBag) {
        if (control instanceof GridViewModel) {
          visitors.set(control, visitor);
          control.rowInitializing.add(rowInitializaing);
          control.rowEditorClosed.add(rowClosed);
        }
      }
    };

    // Инициализация вдимости контролов по визитору при открытии строки таблицы
    const rowInitializaing = (e: GridRowEventArgs) => {
      try {
        if (e.rowModel) {
          initModel(e.rowModel, visitors.get(e.control)!);
        }
      } catch (e) {
        showNotEmpty(ValidationResult.fromError(e));
      }
    };

    // Отписка от созданных подписок при закрытии строки грида
    const rowClosed = (e: GridRowEventArgs) => {
      if (e.rowModel) {
        for (let control of e.rowModel.controlsBag) {
          if (control instanceof GridViewModel) {
            visitors.delete(control);
            control.rowInitializing.remove(rowInitializaing);
            control.rowEditorClosed.remove(rowClosed);
          }
        }
      }
    };

    const extendedSettings = token.extendedCardSettings;
    const sectionSettings = extendedSettings.getCardSettings();
    const tasksSettings = extendedSettings.getTaskSettings();
    const visibilitySettings = new VisibilitySettings();
    visibilitySettings.fill(extendedSettings.getVisibilitySettings());

    const model = context.model;

    if (sectionSettings && sectionSettings.length > 0) {
      hideControlsVisitor.fill(sectionSettings, visibilitySettings);

      initModel(model, hideControlsVisitor);
      if (tasksSettings && tasksSettings.size > 0) {
        this.modifyTask(model, tvm => {
          const taskSettings = tasksSettings.get(tvm.taskModel.cardTask!.typeId);
          if (!taskSettings) {
            return;
          }

          const taskVisitor = new HideControlsVisitor();
          taskVisitor.fill(taskSettings, visibilitySettings);

          tvm.modifyWorkspace(e => {
            initModel(e.task.taskModel, taskVisitor);
          });
        });
      }
    }
  }

  private modifyTask(
    model: ICardModel,
    modifyAction: (tvm: TaskViewModel, m: ICardModel) => void
  ): boolean {
    if (!(model instanceof DefaultFormTabWithTasksViewModel)) {
      return false;
    }

    for (let task of model.tasks.filter(x => x instanceof TaskViewModel)) {
      modifyAction(task, model);
    }

    return true;
  }

}
import * as React from 'react';
declare class SidePanelBlocker extends React.Component<ISidePanelBlockerProps, {}> {
    shouldComponentUpdate(nextProps: any): boolean;
    render(): JSX.Element;
}
export interface ISidePanelBlockerProps {
    isPanelOpen: boolean;
}
export default SidePanelBlocker;

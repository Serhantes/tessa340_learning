import { CardProps } from './card';
export interface LazyCardProps extends CardProps {
    alwaysLoadComponent?: boolean;
}
export interface LazyCardState {
    component: any;
}
declare const _default: any;
export default _default;

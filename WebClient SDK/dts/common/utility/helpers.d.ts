/**
 * Добавить параметр в url-запрос
 * @param {string} query      текущий зарпос
 * @param {string} paramName  название параметра
 * @param {string} paramValue значение
 * @return {string}
 */
export declare function addQueryParam(query: string, paramName: string, paramValue: string | null): string;
/**
 * Проверка, клик/тап был мимо набора элементов
 * @param {Event} event       событие клика/тапа
 * @param {Node[]} ...elements элементы для проверки
 */
export declare function checkIfClickedOutside(event: Event, ...elements: (Node | null)[]): boolean;
/**
 * Капитализирует строку
 * @param  {string} str
 * @return {string}
 */
export declare function capitalizeFirstLetter(str: string): string;
/**
 * Получить FormData из объекта, где все вложенные проперти будут частью FormData.
 *
 * @export
 * @param {Object} object Объект.
 * @param {FormData} [form] Форма.
 * @param {string} [namespace] Неймспейс.
 * @returns {FormData} Форма.
 */
export declare function getFormDataRecursive(object: Object, form?: FormData, namespace?: string): FormData;
/**
 * Это карточный url
 * @param  {string}  path url
 * @return {boolean}
 */
export declare function isCardUrl(path: string): boolean;
export declare function getViewIdFromUrl(path: string): string | undefined;
/**
 * Возвращает id карточки из url
 * @param  {string} path url
 * @return {string}
 */
export declare function getCardIdFromUrl(path: string): string | undefined;
export declare const ScriptRegex: RegExp;
export declare const ALinkRegex: RegExp;
export declare function errorHandler(error: any): any;

import { ICertificate } from '../ICertificate';
declare global {
    interface Window {
        cadesplugin: any;
    }
}
export declare function getCerts(): Promise<ICertificate[]>;
export declare function signFile(certSubjectName: string, fileBase64Str: string): Promise<string>;
export declare function checkSign(signature: string, fileBase64Str: string): Promise<{
    valid: boolean;
    error: string;
}>;

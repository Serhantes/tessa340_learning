import * as React from 'react';
import * as Helpers from 'common/utility';
declare class PopoverInternal extends React.Component<IPopoverInternal, {}> {
    enterTimer: any;
    leaveTimer: any;
    layerIndex: number;
    openDirection: 'right' | 'left';
    static defaultProps: {
        borderOffset: number;
        rootOffset: number;
        horizontalOffset: number;
        openPosition: string;
        openDirection: string;
        up: boolean;
        onlyMountPositionUpdate: boolean;
        instaUnmount: boolean;
    };
    componentWillMount(): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(): void;
    componentWillAppear(callback: any): void;
    componentWillEnter(callback: any): void;
    componentDidAppear(): void;
    componentDidEnter(): void;
    componentWillLeave(callback: any): void;
    updatePosition(popover: any, autoDirection?: boolean): void;
    getPopoverOffests(openDirection: 'right' | 'left', rootOffsetElement: Helpers.IHtmlElementPosition | null): {
        hOffset: number;
        vOffset: number;
    };
    setPopoverOffsets(popover: any, openDirection: 'right' | 'left', offsets: {
        hOffset: number;
        vOffset: number;
    }): void;
    isOverflowPopoverWidth(width: number, hOffset: number): boolean;
    initializeAnimation(callback: any): void;
    animate(value: any, insta?: boolean): void;
    getOpenDirection(): "right" | "left";
    getHorizontalOffset(openPosition: any, openDirection: any, horizontalOffset: any): any;
    handleOutsideClick: (event: any) => void;
    handleContextOutsideClick: (event: any) => void;
    render(): JSX.Element;
}
export interface IPopoverInternal {
    children?: any;
    className?: string;
    style?: object;
    rootElement?: React.ReactInstance;
    borderOffset?: number;
    rootOffset?: number;
    horizontalOffset?: number | any;
    onOutsideClick: any;
    openPosition?: 'left' | 'right';
    openDirection?: 'left' | 'right';
    up?: boolean;
    autoDirection?: boolean;
    onlyMountPositionUpdate?: boolean;
    instaUnmount?: boolean;
    contextMenu?: boolean;
    updateStyle?: (style: CSSStyleDeclaration) => void;
    setStyledComponents?: (props: any) => string;
}
export default PopoverInternal;

import React from 'react';
declare class Popover extends React.Component<PopoverProps, PopoverState> {
    private _el;
    constructor(props: PopoverProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    firstChild(props: PopoverProps): any;
    render(): React.ReactPortal;
}
export interface PopoverState {
    mounted: boolean;
}
export interface PopoverProps {
    children?: any;
    isOpened?: boolean;
    onOutsideClick: any;
    className?: string;
    style?: object;
    rootElement?: any;
    borderOffset?: number;
    rootOffset?: number;
    horizontalOffset?: number | any;
    openPosition?: 'left' | 'right';
    openDirection?: 'left' | 'right';
    up?: boolean;
    autoDirection?: boolean;
    onlyMountPositionUpdate?: boolean;
    instaUnmount?: boolean;
    contextMenu?: boolean;
    updateStyle?: (style: CSSStyleDeclaration) => void;
    setStyledComponents?: (props: any) => string;
    [key: string]: any;
}
export default Popover;

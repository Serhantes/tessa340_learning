import * as React from 'react';
declare class FlatButton extends React.Component<IFlatButtonProps, {}> {
    static defaultProps: {
        labelPosition: string;
    };
    shouldComponentUpdate(nextProps: any): any;
    render(): JSX.Element;
}
export interface IFlatButtonProps {
    children?: any;
    icon?: string | any;
    className?: string;
    disabled?: boolean;
    disableTouchRipple?: boolean;
    label?: string;
    labelPosition?: 'before' | 'after';
    style?: object;
    onClick?: any;
    [key: string]: any;
}
export default FlatButton;

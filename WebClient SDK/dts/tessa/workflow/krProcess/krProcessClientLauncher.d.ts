import { KrProcessInstance } from './krProcessInstance';
import { KrProcessLaunchResult } from './krProcessLaunchResult';
import { ICardEditorModel } from 'tessa/ui/cards';
export declare function launchProcess(krProcess: KrProcessInstance, specificParams?: {
    useCurrentCardEditor?: boolean;
    cardEditor?: ICardEditorModel;
}): Promise<KrProcessLaunchResult>;
export declare function launchProcessWithCardEditor(krProcess: KrProcessInstance, cardEditor: ICardEditorModel): Promise<KrProcessLaunchResult>;

import { StorageObject, IStorage } from 'tessa/platform/storage';
export declare class KrProcessInstance extends StorageObject {
    constructor(processId: guid, cardId?: guid | null, processInfo?: IStorage, features?: string[]);
    private _features;
    readonly processId: guid;
    readonly cardId: guid | null;
    readonly processInfo: IStorage;
    readonly features: string[];
}

import { StorageObject, IStorage } from 'tessa/platform/storage';
import { ValidationResult, ValidationStorageResultBuilder } from 'tessa/platform/validation';
export declare class KrProcessLaunchResult extends StorageObject {
    constructor(storage: IStorage);
    constructor(processId: guid | null, validationResult: ValidationResult);
    readonly processId: guid | null;
    readonly validationResult: ValidationStorageResultBuilder;
}

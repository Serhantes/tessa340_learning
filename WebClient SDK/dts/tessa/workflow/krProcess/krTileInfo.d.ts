import { StorageObject, IStorage } from 'tessa/platform/storage';
export declare class KrTileInfo extends StorageObject {
    constructor(storage?: IStorage);
    static readonly idKey = "ID";
    static readonly nameKey = "Name";
    static readonly captionKey = "Caption";
    static readonly iconKey = "Icon";
    static readonly tooltipKey = "Tooltip";
    static readonly isGlobalKey = "IsGlobal";
    static readonly askConfirmationKey = "AskConfirmation";
    static readonly confirmationMessageKey = "ConfirmationMessage";
    static readonly actionGroupingKey = "ActionGrouping";
    static readonly nestedTilesKey = "NestedTiles";
    readonly id: guid;
    readonly name: string;
    readonly caption: string;
    readonly icon: string;
    readonly tooltip: string;
    readonly isGlobal: boolean;
    readonly askConfirmation: boolean;
    readonly confirmationMessage: string;
    readonly actionGrouping: boolean;
    private _nestedTiles;
    readonly nestedTiles: ReadonlyArray<KrTileInfo>;
}

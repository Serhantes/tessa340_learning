import { StorageObject, IStorage } from 'tessa/platform/storage';
export declare class KrPermissionFileSettings extends StorageObject {
    constructor(storage?: IStorage);
    static readonly fileIdKey: string;
    static readonly accessSettingKey: string;
    id: guid;
    acessSetting: number;
}

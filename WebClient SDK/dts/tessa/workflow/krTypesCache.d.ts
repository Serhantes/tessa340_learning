import { IKrType } from './krType';
import { KrDocType } from './krDocType';
import { KrCardType } from './krCardType';
export declare class KrTypesCache {
    private constructor();
    private static _instance;
    static readonly instance: KrTypesCache;
    private _types;
    private _unavailableTypes;
    readonly docTypes: ReadonlyArray<KrDocType>;
    readonly cardTypes: ReadonlyArray<KrCardType>;
    readonly types: ReadonlyArray<IKrType>;
    readonly unavailableTypes: Set<guid>;
    initialize(types: any): void;
    invalidate(): void;
    private getDocTypes;
    private getKrCardTypes;
    private getCardTypesList;
}

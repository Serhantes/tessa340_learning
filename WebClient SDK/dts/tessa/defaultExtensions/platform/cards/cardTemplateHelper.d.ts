export declare class CardTemplateHelper {
    static readonly CardInTemplateKey: string;
    static readonly TemplateCardKey: string;
    static readonly TemplateSectionRowsKey: string;
    static readonly TemplateOriginalCardIDKey: string;
    static readonly TemplateOriginalPermissions: string;
    static readonly OpeningCardFromTemplateKey: string;
    static readonly PreviousPermissionsKey: string;
}

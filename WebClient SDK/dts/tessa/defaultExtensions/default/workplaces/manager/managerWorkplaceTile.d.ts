import * as React from 'react';
import { ManagerWorkplaceSettings } from './managerWorkplaceSettings';
import { IWorkplaceViewComponent } from 'tessa/ui/views';
export declare class ManagerWorkplaceTileViewModel {
    readonly viewComponent: IWorkplaceViewComponent;
    readonly row: ReadonlyMap<string, any>;
    constructor(viewComponent: IWorkplaceViewComponent, row: ReadonlyMap<string, any>);
    private _activeImage;
    private _hoverImage;
    private _inactiveImage;
    private _hover;
    caption: string;
    count: number;
    activeImage: string;
    hoverImage: string;
    inactiveImage: string;
    hover: boolean;
    readonly isSelected: boolean;
    static create(viewComponent: IWorkplaceViewComponent, row: ReadonlyMap<string, any>, settings: ManagerWorkplaceSettings): Promise<ManagerWorkplaceTileViewModel>;
    selectTile(): void;
}
interface ManagerWorkplaceTileProps {
    viewModel: ManagerWorkplaceTileViewModel;
}
export declare class ManagerWorkplaceTile extends React.Component<ManagerWorkplaceTileProps> {
    render(): JSX.Element;
    private handleMouseEnter;
    private handleMouseLeave;
    private handleClick;
}
export {};

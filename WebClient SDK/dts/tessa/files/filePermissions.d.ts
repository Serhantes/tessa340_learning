export interface IFilePermissions {
    canCopy: boolean;
    canEdit: boolean;
    canModifyCategory: boolean;
    canModifyProperties: boolean;
    canModifyType: boolean;
    canModifyName: boolean;
    canReplace: boolean;
    canRemove: boolean;
    canSign: boolean;
    canUseSignatures: boolean;
    canRemoveSignatures: boolean;
    set(other: IFilePermissions): any;
    clone(): IFilePermissions;
    seal<T = FilePermissionsSealed>(): T;
}
export interface FilePermissionsSealed {
    readonly canCopy: boolean;
    readonly canEdit: boolean;
    readonly canModifyCategory: boolean;
    readonly canModifyProperties: boolean;
    readonly canModifyType: boolean;
    readonly canModifyName: boolean;
    readonly canReplace: boolean;
    readonly canRemove: boolean;
    readonly canSign: boolean;
    readonly canUseSignatures: boolean;
    readonly canRemoveSignatures: boolean;
    set(other: IFilePermissions): any;
    clone(): IFilePermissions;
    seal<T = FilePermissionsSealed>(): T;
}
export declare class FilePermissions implements IFilePermissions {
    constructor();
    private _canCopy;
    private _canEdit;
    private _canModifyCategory;
    private _canModifyProperties;
    private _canModifyType;
    private _canModifyName;
    private _canReplace;
    private _canRemove;
    private _canSign;
    private _canUseSignatures;
    private _canRemoveSignatures;
    canCopy: boolean;
    canEdit: boolean;
    canModifyCategory: boolean;
    canModifyProperties: boolean;
    canModifyType: boolean;
    canModifyName: boolean;
    canReplace: boolean;
    canRemove: boolean;
    canSign: boolean;
    canUseSignatures: boolean;
    canRemoveSignatures: boolean;
    set(other: IFilePermissions): void;
    clone(): IFilePermissions;
    seal<T = FilePermissionsSealed>(): T;
    static readonly full: FilePermissionsSealed;
    static readonly empty: FilePermissionsSealed;
}

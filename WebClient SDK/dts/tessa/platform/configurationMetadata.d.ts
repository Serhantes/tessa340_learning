import { IStorage } from 'tessa/platform/storage';
export declare class ConfigurationMetadata {
    private storage;
    constructor(storage?: IStorage);
    readonly flags: number;
    readonly isUnknown: boolean;
    readonly buildVersion: string;
    readonly buildName: string;
    readonly buildDate: string;
    readonly description: string;
    readonly modified: string;
    readonly modifiedByID: string;
    readonly modifiedByName: string;
    readonly version: number;
    readonly info: IStorage;
}

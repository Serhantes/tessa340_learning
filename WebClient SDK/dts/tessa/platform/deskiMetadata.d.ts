export declare class DeskiMetadata {
    private _enabled?;
    private _port?;
    private _info?;
    constructor(storage: {
        enabled: boolean;
        port?: number;
        info?: DeskiInfo;
    });
    readonly enabled: boolean;
    readonly port: number | undefined;
    readonly info: DeskiInfo | undefined;
}
export interface DeskiInfo {
    FullName: string;
    ShortName: string;
    VerMajor: number;
    VerMinor: number;
}

import { ValidationResultItem } from './validationResultItem';
import { ValidationResultType } from './validationResultType';
import { ValidationLevel } from './validationLevel';
export declare class ValidationResult {
    constructor(items: ValidationResultItem[]);
    readonly items: ReadonlyArray<ValidationResultItem>;
    readonly hasErrors: boolean;
    readonly hasWarnings: boolean;
    readonly hasInfo: boolean;
    readonly isSuccessful: boolean;
    static readonly empty: ValidationResult;
    format(): string;
    toString(level?: ValidationLevel): string;
    static fromError(error: Error | any, warning?: boolean): ValidationResult;
    static fromText(text: string, type?: ValidationResultType): ValidationResult;
}

export declare class ScopeContext<TContext> {
    private _current;
    readonly context: TContext | null;
    readonly hasContext: boolean;
    readonly current: ScopeContextInstance<TContext> | null;
    readonly parent: ScopeContextInstance<TContext> | null;
    create(context: TContext | null, disposeAction?: ((context: TContext | null) => void) | null): ScopeContextInstance<TContext>;
    setCurrentContext(current: ScopeContextInstance<TContext> | null): void;
}
export declare class ScopeContextInstance<TContext> {
    private _scopeContext;
    private _context;
    private _parent;
    private _disposeAction;
    readonly context: TContext | null;
    readonly parent: ScopeContextInstance<TContext> | null;
    initialize(scopeContext: ScopeContext<TContext>, context: TContext | null, disposeAction?: ((context: TContext | null) => void) | null): void;
    dispose(): void;
}

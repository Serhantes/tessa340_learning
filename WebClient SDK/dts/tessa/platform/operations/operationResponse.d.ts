import { ValidationInfoStorageObject, ValidationStorageResultBuilder } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class OperationResponse extends ValidationInfoStorageObject {
    constructor(storage?: IStorage);
    static validationResultKey: string;
    validationResult: ValidationStorageResultBuilder;
    tryGetValidationResult(): ValidationStorageResultBuilder | null | undefined;
}

import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
import { ValidationInfoStorageObject, ValidationStorageResultBuilder } from 'tessa/platform/validation';
import { Dbms } from 'tessa/platform/dbms';
import { Card } from 'tessa/cards';
export declare class MainInitializationResponse extends ValidationInfoStorageObject implements ICloneable<MainInitializationResponse> {
    constructor(storage?: IStorage);
    static readonly dbmsKey: string;
    static readonly personalRoleSatelliteKey: string;
    static readonly validationResultKey: string;
    static readonly configurationCacheIsActualKey: string;
    static readonly supportedPdfFormatsKey: string;
    static readonly webSettingsKey: string;
    static readonly additionalMetaKey: string;
    dbms: Dbms;
    personalRoleSatellite: Card;
    validationResult: ValidationStorageResultBuilder;
    configurationCacheIsActual: boolean;
    supportedPdfFormats: string[];
    webSettings: IStorage;
    readonly additionalMeta: IStorage;
    tryGetPersonalRoleSatellite(): Card | null | undefined;
    tryGetValidationResult(): ValidationStorageResultBuilder | null | undefined;
    tryGetSupportedPdfFormats(): string[] | null | undefined;
    tryGetWebSettings(): IStorage | null | undefined;
    clone(): MainInitializationResponse;
}

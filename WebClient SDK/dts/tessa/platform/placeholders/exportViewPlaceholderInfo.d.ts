import { ViewPlaceholderInfo } from './viewPlaceholderInfo';
import { SerializationTransform } from 'tessa/platform/serialization';
export declare function asSplitedString(transformAction: () => SerializationTransform): () => SerializationTransform;
export declare class ExportViewPlaceholderInfo extends ViewPlaceholderInfo {
    constructor();
    private _exportAll;
    private _title;
    columnsOrdering: string[];
    exportAll: boolean;
    title: string;
}

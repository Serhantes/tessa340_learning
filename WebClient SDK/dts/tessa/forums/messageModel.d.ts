import { MessageModelBase } from './messageModelBase';
import { MessageTypes, OutsideTypes } from './enums';
import { AvatarSource } from './avatarSource';
import { ItemModel } from './itemModel';
import { IStorage, IStorageValueFactory, ArrayStorage } from 'tessa/platform/storage';
export declare class MessageModel extends MessageModelBase {
    constructor(storage?: IStorage);
    static readonly imageSourceKey: string;
    static readonly replyKey: string;
    static readonly createdKey: string;
    static readonly typeIdKey: string;
    static readonly lastNameKey: string;
    static readonly firstNameKey: string;
    static readonly outsideKey: string;
    static readonly attachmentsKey: string;
    imageSource: AvatarSource;
    reply: guid | null;
    created: string | null;
    typeId: MessageTypes;
    lastName: string | null;
    firstName: string | null;
    outside: OutsideTypes;
    attachments: ArrayStorage<ItemModel>;
    tryGetImageSource(): AvatarSource | null | undefined;
    tryGetAttachments(): ArrayStorage<ItemModel> | null | undefined;
    private static readonly _attachmentFactory;
}
export declare class MessageModelFactory implements IStorageValueFactory<MessageModel> {
    getValue(storage: IStorage): MessageModel;
    getValueAndStorage(): {
        value: MessageModel;
        storage: IStorage;
    };
}

import { TopicModel } from './topicModel';
import { MessageModel } from './messageModel';
import { CardResponseBase } from 'tessa/cards/service';
import { ICloneable } from 'tessa/platform';
import { IStorage, ArrayStorage } from 'tessa/platform/storage';
export declare class ForumResponse extends CardResponseBase implements ICloneable<ForumResponse> {
    constructor(storage?: IStorage);
    static readonly forumSettingsKey: string;
    static readonly satelliteIdKey: string;
    static readonly topicsKey: string;
    static readonly messagesKey: string;
    forumSettings: IStorage;
    satelliteId: guid | null;
    topics: ArrayStorage<TopicModel>;
    messages: ArrayStorage<MessageModel>;
    tryGetTopics(): ArrayStorage<TopicModel> | null | undefined;
    tryGetMessages(): ArrayStorage<MessageModel> | null | undefined;
    private static readonly _topicsFactory;
    private static readonly _messageFactory;
    clone(): ForumResponse;
}

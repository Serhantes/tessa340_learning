import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class UserStatusModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly userIdKey: string;
    static readonly topicIdKey: string;
    static readonly lastReadMessageTimeKey: string;
    static readonly preLastVisitedAtKey: string;
    userId: guid | null;
    topicId: guid | null;
    lastReadMessageTime: string | null;
    preLastVisitedAt: string | null;
}

import * as React from 'react';
import { CardTypeFormSealed } from 'tessa/cards/types';
import { ICardModel, IFormViewModel } from 'tessa/ui/cards';
import { UIButton } from 'tessa/ui/uiButton';
export interface CustomFormDialogProps {
    form: IFormViewModel;
    buttons: UIButton[];
    onClose: (result: any) => void;
    backgroundHolder: boolean;
}
export declare class CustomFormDialog extends React.Component<CustomFormDialogProps> {
    render(): JSX.Element;
    private renderForm;
    private renderButtons;
    private handleClose;
}
export declare function showFormDialog(form: CardTypeFormSealed, model: ICardModel, initializeAction?: ((form: IFormViewModel) => void) | null, buttons?: UIButton[], backgroundHolder?: boolean): Promise<any>;

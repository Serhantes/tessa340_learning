import { IViewContext } from './viewContext';
import { IReactRefsProvider, ReactRef } from '../reactRefProvider';
export declare class WorkplaceLayoutViewModel implements IReactRefsProvider {
    constructor(parent?: WorkplaceLayoutViewModel | null, _splitPosition?: number | null, // TODO
    _secondChildSize?: number | null);
    private _firstChild;
    private _secondChild;
    private _innerContent;
    private _caption;
    readonly parent: WorkplaceLayoutViewModel | null;
    firstChild: WorkplaceLayoutViewModel | null;
    secondChild: WorkplaceLayoutViewModel | null;
    innerContent: any | null;
    caption: string;
    selectionAction: (context: IViewContext | null) => void;
    readonly isSelectionListener: boolean;
    getReactRefs(): ReactRef[];
}

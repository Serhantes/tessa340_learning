import * as React from 'react';
export interface DynamicContentComponentProps {
    viewModel: any;
}
export declare class DynamicContentComponent extends React.Component<DynamicContentComponentProps> {
    private _component;
    componentWillMount(): void;
    componentWillUpdate(nextProps: DynamicContentComponentProps): void;
    render(): JSX.Element | null;
}

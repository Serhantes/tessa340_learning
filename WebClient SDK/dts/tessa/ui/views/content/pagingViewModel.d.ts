import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
export declare class PagingViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    currentPage: number;
    readonly isOptionalPagingEnabled: boolean;
    readonly isPagingEnabled: boolean;
    optionalPagingStatus: boolean;
    readonly pageCount: number;
    readonly pageCountVisible: Visibility;
    readonly hasNextPage: boolean;
    readonly hasPreviousPage: boolean;
    readonly isLoading: boolean;
}

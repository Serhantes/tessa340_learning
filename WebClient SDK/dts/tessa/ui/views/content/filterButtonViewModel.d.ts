import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { IViewParameters } from '../parameters';
import { Visibility } from 'tessa/platform';
import { ViewMetadataSealed } from 'tessa/views/metadata';
export declare class FilterButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, parameters: IViewParameters, viewMetadata: ViewMetadataSealed, area?: ContentPlaceArea, order?: number);
    private _visibility;
    readonly parameters: IViewParameters;
    readonly viewMetadata: ViewMetadataSealed;
    visibility: Visibility;
    readonly isLoading: boolean;
    openFilter(): Promise<void>;
}

import { TableGridViewModel } from './tableGridViewModel';
import { TableCellViewModel } from './tableCellViewModel';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class TableRowViewModel {
    readonly data: ReadonlyMap<string, any>;
    readonly grid: TableGridViewModel;
    readonly appearance: string | null;
    private _getContextMenu;
    readonly isGroup: boolean;
    constructor(data: ReadonlyMap<string, any>, grid: TableGridViewModel, appearance: string | null, _getContextMenu: (() => ReadonlyArray<MenuAction>) | null, treeId: string | null, treeParentRowId: string | null, isGroup: boolean, isToggle: boolean);
    private _cells;
    private _cellsByColumnName;
    private _isToggled;
    readonly id: guid;
    readonly cells: ReadonlyArray<TableCellViewModel>;
    readonly isSelected: boolean;
    readonly isLastSelected: boolean;
    isToggled: boolean;
    blockId: string | null;
    parentRowId: string | null;
    getByIndex(index: number): TableCellViewModel | null;
    getByName(name: string): TableCellViewModel | null;
    initialize(cells: ReadonlyMap<string, TableCellViewModel>): void;
    selectRow(isSelected?: boolean): void;
    doubleClickAction: (cell?: TableCellViewModel | null) => void;
    getContextMenu(): ReadonlyArray<MenuAction>;
    toggle(): void;
}

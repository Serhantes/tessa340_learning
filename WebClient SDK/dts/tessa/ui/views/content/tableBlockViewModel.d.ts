export declare class TableBlockViewModel {
    readonly id: string;
    readonly caption: string;
    readonly parentBlockId: string | null;
    constructor(id: string, caption: string, parentBlockId?: string | null);
    private _isToggled;
    private _count;
    isToggled: boolean;
    count: number;
    readonly text: string;
    toggle(): void;
}

/// <reference types="react" />
import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
export declare class QuickSearchViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _searchText;
    private _visibility;
    private _reactComponentRef;
    quickSearchEnabled: boolean;
    searchText: string;
    visibility: Visibility;
    readonly isLoading: boolean;
    focusControlWhenDataWasLoaded: boolean;
    dispose(): void;
    bindReactComponentRef(ref: React.RefObject<any>): void;
    unbindReactComponentRef(): void;
    focus(opt?: FocusOptions): void;
    search(): void;
}

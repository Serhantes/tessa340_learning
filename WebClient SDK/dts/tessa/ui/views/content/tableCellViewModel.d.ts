import { TableRowViewModel } from './tableRowViewModel';
import { TableColumnViewModel } from './tableColumnViewModel';
export declare class TableCellViewModel {
    readonly row: TableRowViewModel;
    readonly column: TableColumnViewModel;
    readonly value: any;
    readonly appearance: string | null;
    constructor(row: TableRowViewModel, column: TableColumnViewModel, value: any, appearance: string | null);
    private _convertedValue;
    private _toolTip;
    readonly maxLength: number;
    readonly convertedValue: any;
    readonly isSelected: boolean;
    readonly toolTip: string;
    private convertValue;
    private sliceValue;
    selectCell(isSelected?: boolean): void;
}

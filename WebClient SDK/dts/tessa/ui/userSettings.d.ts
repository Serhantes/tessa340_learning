import { IStorage } from 'tessa/platform/storage';
export interface IUserSettings {
    data: IStorage;
    taskColor?: string;
    topicItemColor?: string;
    authorColor?: string;
    authorDeputyColor?: string;
    performerColor?: string;
    performerDeputyColor?: string;
}
export declare class UserSettings implements IUserSettings {
    _data: IStorage<any>;
    _taskColor?: string;
    _topicItemColor?: string | undefined;
    _authorColor?: string | undefined;
    _authorDeputyColor?: string | undefined;
    _performerColor?: string | undefined;
    _performerDeputyColor?: string | undefined;
    data: IStorage;
    taskColor: string | undefined;
    topicItemColor: string | undefined;
    authorColor: string | undefined;
    authorDeputyColor: string | undefined;
    performerColor: string | undefined;
    performerDeputyColor: string | undefined;
    private clearFields;
}

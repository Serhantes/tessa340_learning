import { ITile } from './interfaces';
export declare class TileEvaluationAction {
    constructor(tile: ITile, isCollapsed?: boolean, isEnabled?: boolean, isHidden?: boolean);
    readonly tile: ITile;
    readonly isCollapsed?: boolean;
    readonly isCollapsedEffective: boolean;
    readonly isEnabled?: boolean;
    readonly isEnabledEffective: boolean;
    readonly isHidden?: boolean;
    readonly isHiddenEffective: boolean;
    apply(): void;
}

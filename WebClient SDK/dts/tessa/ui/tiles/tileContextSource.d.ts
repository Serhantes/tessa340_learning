import { HotkeyStorage } from './hotkeyStorage';
import { IUIContext } from 'tessa/ui/uiContext';
export declare class TileContextSource {
    constructor(context?: IUIContext);
    private _context;
    context: IUIContext;
    readonly hotkeyStorage: HotkeyStorage;
}

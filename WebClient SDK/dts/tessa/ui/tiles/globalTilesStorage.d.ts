import { TileWorkspace } from './tileWorkspace';
export declare class GlobalTilesStorage {
    private constructor();
    private static _instance;
    static readonly instance: GlobalTilesStorage;
    readonly workspace: TileWorkspace;
}

import { FieldMapStorage } from 'tessa/cards/fieldMapStorage';
export declare class NumeratorDialogViewModel {
    constructor(fields: FieldMapStorage, fullNumberFieldName: string, numberFieldName: string, sequenceFieldName: string | null, isReadOnly: boolean, sequenceIsVisible: boolean);
    private readonly _fields;
    private readonly _fullNumberFieldName;
    private readonly _numberFieldName;
    private readonly _sequenceFieldName;
    readonly isReadOnly: boolean;
    readonly sequenceIsVisible: boolean;
    readonly sequenceWarningIsVisible: boolean;
    readonly serialNumberWarningIsVisible: boolean;
    fullNumber: string | null;
    number: string | null;
    sequence: string | null;
}

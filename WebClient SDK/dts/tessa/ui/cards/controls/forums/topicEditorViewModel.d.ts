import { ForumViewModel } from './forumViewModel';
import { TopicViewModel } from './topicViewModel';
import { IAttachmentMenuContext } from './forumCommon';
import { ForumItemViewModel } from './forumItemViewModel';
import { UIButton } from 'tessa/ui/uiButton';
import { MenuAction } from 'tessa/ui/menuAction';
export interface ForumPager {
    readonly currentPage: number;
    readonly pageCount: number;
    readonly hasPreviousPage: boolean;
    readonly hasNextPage: boolean;
    nextMessages: () => Promise<void>;
    prevMessages: () => Promise<void>;
}
export declare class TopicEditorViewModel implements ForumPager {
    constructor(forumViewModel: ForumViewModel, model: TopicViewModel);
    readonly forumViewModel: ForumViewModel;
    private readonly _topic;
    private _leftButtons;
    private _rightButtons;
    private _currentPage;
    private _pageCount;
    private _hasPreviousPage;
    private _hasNextPage;
    private _separatorMessageId;
    readonly topic: TopicViewModel;
    readonly leftButtons: UIButton[];
    readonly rightButtons: UIButton[];
    readonly attachmentContextMenuGenerators: ((ctx: IAttachmentMenuContext) => void)[];
    readonly currentPage: number;
    readonly pageCount: number;
    readonly hasPreviousPage: boolean;
    readonly hasNextPage: boolean;
    readonly separatorMessageId: guid | null;
    private initDefaultButtons;
    private initAttachmentContextMenu;
    sendMessage(message: string): Promise<void>;
    private enablePrevNextButton;
    private rearrangePages;
    nextMessages(): Promise<void>;
    prevMessages(): Promise<void>;
    private initSeparator;
    getAttachmentContextMenu(item: ForumItemViewModel): MenuAction[];
    dispose(): void;
}

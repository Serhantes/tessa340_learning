import { ForumViewModel } from './forumViewModel';
import { MessageViewModel } from './messageViewModel';
import { ItemModel } from 'tessa/forums';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class ForumItemViewModel {
    constructor(forumViewModel: ForumViewModel, message: MessageViewModel, model: ItemModel);
    private _caption;
    readonly forumViewModel: ForumViewModel;
    readonly message: MessageViewModel;
    readonly model: ItemModel;
    caption: string;
    getContextMenu: () => MenuAction[];
    downloadAttachment(): Promise<void>;
}

import { ForumViewModel } from './forumViewModel';
import { MessageViewModelBase } from './messageViewModelBase';
import { TopicModel } from 'tessa/forums';
export declare abstract class TopicViewModelBase {
    constructor(forumViewModel: ForumViewModel, model: TopicModel);
    protected readonly _messages: MessageViewModelBase[];
    protected _title: string;
    protected _authorName: string;
    protected _created: string;
    protected _description: string;
    readonly forumViewModel: ForumViewModel;
    readonly model: TopicModel;
    readonly id: guid;
    readonly messages: MessageViewModelBase[];
    title: string;
    authorName: string;
    created: string;
    description: string;
}

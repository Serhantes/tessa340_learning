import { MessageViewModelBase } from './messageViewModelBase';
import { ForumViewModel } from './forumViewModel';
import { ForumAvatarViewModel } from './forumAvatarViewModel';
import { ForumItemViewModel } from './forumItemViewModel';
import { TopicViewModelBase } from './topicViewModelBase';
import { MessageModel } from 'tessa/forums';
export declare class MessageViewModel extends MessageViewModelBase {
    constructor(forumViewModel: ForumViewModel, topic: TopicViewModelBase, model: MessageModel);
    private _authorName;
    private _created;
    private _avatar;
    private _items;
    private _innerItems;
    authorName: string;
    created: string;
    avatar: ForumAvatarViewModel;
    readonly items: ForumItemViewModel[];
    readonly innerItems: ForumItemViewModel[];
    private initAttachments;
}

import { Card } from 'tessa/cards';
export declare class ForumPermissionsProvider {
    isEnableAddTopic(_card: Card): boolean;
    isEnableSuperModeratorMode(_card: Card): boolean;
}

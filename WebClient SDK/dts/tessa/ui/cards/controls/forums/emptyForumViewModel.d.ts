import { ForumItemViewModel } from './forumItemViewModel';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class EmptyForumViewModel {
    getAttachmentContextMenu(_item: ForumItemViewModel): MenuAction[];
    dispose(): void;
}

export interface IAutoCompletePopupItem {
    fields: any[];
    displayFields: string[];
    dateTypes: number[];
}
export declare class AutoCompletePopupItem implements IAutoCompletePopupItem {
    constructor(fields: any[], dateTypes?: number[]);
    fields: any[];
    displayFields: string[];
    dateTypes: number[];
    private generateDisplayFields;
}

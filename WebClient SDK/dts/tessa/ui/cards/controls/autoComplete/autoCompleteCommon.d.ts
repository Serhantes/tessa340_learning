import { IAutoCompleteItem } from './autoCompleteItem';
export declare class AutoCompleteValueEventArgs<T> {
    constructor(item: IAutoCompleteItem, autocomplete: T);
    readonly item: IAutoCompleteItem;
    readonly autocomplete: T;
}

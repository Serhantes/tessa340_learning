import { ControlViewModelBase } from './controlViewModelBase';
import { CardTypeControl } from 'tessa/cards/types';
/**
 * Базовый объект для модели представления для элемента управления,
 * выполняющего вывод текстовых строк.
 */
export declare abstract class TextBlockViewModelBase extends ControlViewModelBase {
    constructor(control: CardTypeControl);
    protected _textFontSize: string;
    protected _textColor: string;
    protected _backgroundColor: string;
    protected _borderColor: string;
    protected _borderWidth: string;
    protected _borderRadius: string;
    protected _padding: string;
    textFontSize: string;
    textColor: string;
    backgroundColor: string;
    borderColor: string;
    borderWidth: string;
    borderRadius: string;
    padding: string;
}

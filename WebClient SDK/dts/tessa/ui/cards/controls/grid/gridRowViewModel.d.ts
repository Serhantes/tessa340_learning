import { GridCellViewModel } from './gridCellViewModel';
import { GridColumnInfo } from './gridColumnInfo';
import { Card, CardRow, CardRowState } from 'tessa/cards';
import { ISelectionState } from 'tessa/ui/views/selectionState';
export declare class GridRowViewModel {
    constructor(row: CardRow, card: Card, columnInfos: GridColumnInfo[], orderColumn: string | null, selectionState: ISelectionState);
    private _row;
    private _cells;
    private _state;
    private _orderColumn;
    private _selectionState;
    readonly row: CardRow;
    readonly cells: ReadonlyArray<GridCellViewModel>;
    readonly rowId: guid;
    readonly state: CardRowState;
    readonly order: number;
    isSelected: boolean;
    readonly isLastSelected: boolean;
    clean(): void;
    private onStateChanged;
    changeColumnOrderByTarget(sourceIndex: number, targetIndex: number): void;
}

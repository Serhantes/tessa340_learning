/// <reference types="react" />
import { IBlockViewModel, IControlViewModel, IControlState } from '../interfaces';
import { SupportUnloadingViewModel } from '../supportUnloadingViewModel';
import { CardTypeControl, CardTypeControlSealed } from 'tessa/cards/types';
import { Visibility } from 'tessa/platform/visibility';
import { IValidationResultBuilder } from 'tessa/platform/validation';
import { HorizontalAlignment, VerticalAlignment, Margin, EventHandler } from 'tessa/platform';
import { MediaStyle } from 'ui/mediaStyle';
import { Theme } from 'tessa/ui/themes';
export declare abstract class ControlViewModelBase extends SupportUnloadingViewModel implements IControlViewModel {
    constructor(control: CardTypeControl);
    protected _block: IBlockViewModel;
    protected _caption: string;
    protected _tooltip: string;
    protected _captionVisibility: Visibility;
    protected _captionStyle: MediaStyle | null;
    protected _controlVisibility: Visibility;
    protected _controlStyle: MediaStyle | null;
    protected _margin: Margin | null;
    protected _minWidth: string | null;
    protected _maxWidth: string | null;
    protected _columnSpan: number;
    protected _emptyColumnsToTheLeft: number;
    protected _horizontalAlignment: HorizontalAlignment;
    protected _verticalAlignment: VerticalAlignment;
    protected _startAtNewLine: boolean;
    protected _isRequired: boolean;
    protected _isReadonly: boolean;
    protected _isSpanned: boolean;
    protected _isFocused: boolean;
    protected _hasActiveValidation: boolean;
    protected _reactComponentRef: React.RefObject<any> | null;
    private _themeReaction;
    private _errorAtom;
    readonly componentId: guid;
    /**
     * Информация о типе отображаемого элемента управления.
     */
    readonly cardTypeControl: CardTypeControlSealed;
    /**
     * Имя элемента управления, по которому он доступен в коллекции, или null,
     * если у элемента управления не задано имя.
     */
    readonly name: string | null;
    /**
     * Блок, в котором размещён текущий элемент управления.
     */
    readonly block: IBlockViewModel;
    /**
     * Заголовок элемента управления.
     */
    caption: string;
    /**
     * Всплывающая подсказка для элемента управления или null, если подсказка отсутствует.
     */
    tooltip: string;
    /**
     * Видимость заголовка элемента управления.
     */
    captionVisibility: Visibility;
    /**
     * Стиль заголовка элемента управления.
     */
    captionStyle: MediaStyle | null;
    /**
     * Видимость элемента управления.
     */
    controlVisibility: Visibility;
    /**
     * Стиль элемента управления.
     */
    controlStyle: MediaStyle | null;
    /**
     * Заданный в настройках отступ элемента управления относительно других элементов управления.
     * По умолчанию отступ отсутствует.
     */
    margin: Margin | null;
    /**
     * Минимальная ширина контрола. По умолчанию значение равно 0 и не может быть меньше.
     * Пример: 10px
     */
    minWidth: string | null;
    /**
     * Максимальная ширина контрола.
     * Пример: 10px
     */
    maxWidth: string | null;
    /**
     * Количество колонок, которые занимает контрол по горизонтали. Неактуально для контролов,
     * растягиваемых по ширине всей строки. По умолчанию значение равно 1 и не может быть меньше.
     * Если заданное количество колонок больше, чем общее количество колонок в блоке,
     * то контрол растягивается на ширину строки.
     */
    columnSpan: number;
    /**
     * Количество пустых колонок слева.
     */
    emptyColumnsToTheLeft: number;
    /**
     * Выравнивание по горизонтали элемента управления.
     */
    horizontalAlignment: HorizontalAlignment;
    /**
     * Выравнивание по вертикали элемента управления.
     */
    verticalAlignment: VerticalAlignment;
    /**
     * Признак того, что текущий контрол в блоке всегда начинается с новой строки.
     */
    startAtNewLine: boolean;
    /**
     * Признак того, что элемент управления не содержит отображаемых данных.
     */
    readonly isEmpty: boolean;
    /**
     * Признак того, что элемент управления отмечен, как обязательный для заполнения.
     */
    isRequired: boolean;
    /**
     * Текст валидации обязательного для заполнения элемента.
     */
    requiredText: string;
    /**
     * Признак того, что элемент управления доступен только для чтения
     * или не содержит редактируемых данных.
     */
    isReadOnly: boolean;
    /**
     * Признак того, что элемент управления должен быть растянут
     * на ширину колонки при выводе в несколько колонок.
     */
    isSpanned: boolean;
    /**
     * Признак того, что элемент управления имеет логический фокус.
     */
    isFocused: boolean;
    /**
     * Признак того, что в элементе управления следует включить активную валидацию.
     * При этом если для элемента управления введено некорректное значение, то он будет
     * уведомлять об этом рамкой валидации. Значение устанавливливается равным true обычно
     * после неудачного сохранения карточки. По умолчанию значение равно false.
     */
    hasActiveValidation: boolean;
    /**
     * Функция валидации, проверяющая элемент управления на корректность его значения, или null,
     * если дополнительные проверки значения отсутствуют. Проверка на незаполненное значение всё
     * равно выполняется, если элемент управления был отмечен как обязательный
     * для заполнения (в т.ч. посредством валидатора).
     */
    validationFunc: ((control: IControlViewModel) => string | null) | null;
    /**
     * Сообщение об ошибке, связанное с текущим объектом, или null, если ошибки нет.
     */
    readonly error: string | null;
    readonly hasEmptyValue: boolean;
    bindReactComponentRef(ref: React.RefObject<any>): void;
    unbindReactComponentRef(): void;
    /**
     * Метод focus() вызывает нативный метод элемента. Поэтому для правильной работы метода,
     * необходимо чтобы контрол уже был отрендерен.
     * Если необходимо вызвать focus() при открытии карточки, то нужно вызывать его в CardUIExtension.contextInitialized.
     * В этом методе карточка уже будет отрисована и все контролы будут иметь ссылки на свои html элементы.
     * Если необходимы вызвать при открытии формы контрола таблицы, то нужно вызывать его в GridViewModel.rowInitialized.
     */
    focus(opt?: FocusOptions): void;
    setBlock(block: IBlockViewModel): void;
    getState(): IControlState | null;
    setState(_state: IControlState): boolean;
    protected onUnloading(_validationResult: IValidationResultBuilder): void;
    getCaptionStyle(): MediaStyle | null;
    getControlStyle(): MediaStyle | null;
    notifyUpdateValidation(): void;
    readonly onThemeChanged: EventHandler<(args: ThemeChangedEventArgs) => void>;
}
export interface ThemeChangedEventArgs {
    theme: Theme;
}

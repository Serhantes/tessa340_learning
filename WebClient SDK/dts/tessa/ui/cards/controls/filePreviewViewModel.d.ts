import { ControlViewModelBase } from './controlViewModelBase';
import { CardTypeControl } from 'tessa/cards/types';
import { IFilePreviewControl, IFileControl, IFileControlManager } from 'tessa/ui/files';
/**
 * Элемент управления предпросмотром в карточке.
 */
export declare class FilePreviewViewModel extends ControlViewModelBase implements IFilePreviewControl {
    constructor(control: CardTypeControl, minHeight?: number, maxHeight?: number);
    private _minHeight?;
    /**
     * Минимальная высота элемента управления. Укажите <c>double.NaN</c>, чтобы не ограничивать размер.
     */
    minHeight: number | undefined;
    private _maxHeight?;
    /**
     * Максимальная высота элемента управления. Укажите <c>double.NaN</c>, чтобы не ограничивать размер.
     */
    maxHeight: number | undefined;
    /**
     * Объект, управляющий доступностью предпросмотра.
     */
    fileControlManager: IFileControlManager;
    attach(fileControl: IFileControl): void;
}

import { FileViewModel } from '../fileViewModel';
export declare abstract class FileFiltering {
    constructor(name: string, caption: string, isCollapsed?: boolean);
    readonly name: string;
    readonly caption: string;
    isCollapsed: boolean;
    abstract isVisible(viewModel: FileViewModel): boolean;
    abstract equals(other: FileFiltering): boolean;
}

import { IFormViewModel, IBlockViewModel, ICardModel, IFormState } from '../interfaces';
import { CardTypeFormSealed } from 'tessa/cards/types';
import { SupportUnloadingViewModel } from 'tessa/ui/cards/supportUnloadingViewModel';
import { IValidationResultBuilder } from 'tessa/platform/validation';
import { PreviewManager } from 'tessa/ui/cards/previewManager';
import { Visibility, EventHandler } from 'tessa/platform';
import { CardFilePreviewPosition } from 'tessa/cards';
export declare class PreviewFormViewModel extends SupportUnloadingViewModel implements IFormViewModel {
    private static _sideAtom;
    private static _widthAtom;
    constructor(model: ICardModel);
    protected _blocks: ReadonlyArray<IBlockViewModel>;
    protected _tabCaption: string | null;
    protected _blockMargin: string | null;
    protected _isForceTabMode: boolean;
    protected _contentClass: string;
    protected _visibility: Visibility;
    readonly cardModel: ICardModel;
    readonly previewManager: PreviewManager;
    readonly componentId: guid;
    readonly cardTypeForm: CardTypeFormSealed;
    readonly blocks: ReadonlyArray<IBlockViewModel>;
    readonly name: string | null;
    readonly isEmpty: boolean;
    tabCaption: string | null;
    blockMargin: string | null;
    isForceTabMode: boolean;
    readonly headerClass: string;
    readonly contentClass: string;
    isEnabled: boolean;
    readonly hasFileControl: boolean;
    readonly filePreviewIsDisabled: boolean;
    visibility: Visibility;
    getIsTabMode(): boolean;
    getState(): IFormState;
    setState(state: IFormState): boolean;
    close(): boolean;
    static setFilePreviewPosition(value?: CardFilePreviewPosition): void;
    static readonly filePreviewPosition: CardFilePreviewPosition;
    static setFilePreviewWidth(value: number): void;
    static readonly filePreviewWidth: number;
    onUnloading(validationResult: IValidationResultBuilder): void;
    readonly closed: EventHandler<() => void>;
}
export declare class PreviewFormViewModelState implements IFormState {
    constructor(form: PreviewFormViewModel);
    readonly isForceTabMode: boolean;
    readonly visibility: Visibility;
    apply(form: PreviewFormViewModel): boolean;
}
export interface PreviewFormViewModelSettings {
    previewSide?: CardFilePreviewPosition;
    previewWidth?: number;
}

import { FormTypeBase } from './formTypeBase';
import { ICardModel, IFormViewModel } from '../interfaces';
import { CardTypeForm, CardTypeControl } from 'tessa/cards/types';
/**
 * Тип формы, используемой в автоматическом UI карточки по умолчанию.
 */
export declare class DefaultFormType extends FormTypeBase {
    static readonly formClass: string;
    protected createFormCore(form: CardTypeForm, parentControl: CardTypeControl | null, model: ICardModel): IFormViewModel;
}

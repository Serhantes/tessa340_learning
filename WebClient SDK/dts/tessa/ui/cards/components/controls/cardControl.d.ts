import * as React from 'react';
import { IControlViewModel } from 'tessa/ui/cards/interfaces';
export interface CardControlProps {
    viewModel: IControlViewModel;
}
export declare class CardControl extends React.Component<CardControlProps> {
    private _controlComponent;
    componentWillMount(): void;
    componentWillUpdate(nextProps: CardControlProps): void;
    render(): JSX.Element;
}

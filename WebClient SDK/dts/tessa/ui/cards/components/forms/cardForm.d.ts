import * as React from 'react';
import { IFormViewModel } from 'tessa/ui/cards/interfaces';
export interface CardFormProps {
    viewModel: IFormViewModel;
}
export declare class CardForm extends React.Component<CardFormProps> {
    private _formComponent;
    componentWillMount(): void;
    componentWillUpdate(nextProps: CardFormProps): void;
    render(): JSX.Element;
}

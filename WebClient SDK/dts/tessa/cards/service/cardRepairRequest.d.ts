import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage } from 'tessa/platform/storage';
import { Card } from 'tessa/cards/card';
import { CardNewMode } from 'tessa/cards/cardNewMode';
export declare class CardRepairRequest extends CardInfoStorageObject {
    constructor(storage?: IStorage);
    static readonly newModeKey: string;
    static readonly notifyFieldsUpdatedKey: string;
    static readonly cardKey: string;
    static readonly cardBSONKey: string;
    static readonly convertFromBSONBeforeRequestKey: string;
    static readonly convertFromBSONAfterRequestKey: string;
    newMode: CardNewMode;
    notifyFieldsUpdated: boolean;
    card: Card;
    cardBSON: string;
    convertFromBSONBeforeRequest: boolean;
    convertFromBSONAfterRequest: boolean;
    tryGetCard(): Card | null | undefined;
}

import { CardRequestBase } from './cardRequestBase';
import { IStorage } from 'tessa/platform/storage';
export declare abstract class CardFileRequestBase extends CardRequestBase {
    constructor(storage?: IStorage);
    static readonly fileIdKey: string;
    static readonly fileNameKey: string;
    static readonly fileTypeIdKey: string;
    static readonly fileTypeNameKey: string;
    fileId: guid | null;
    fileName: string | null;
    fileTypeId: guid | null;
    fileTypeName: string | null;
    clean(): void;
}

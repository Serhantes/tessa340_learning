import { CardRequestBase } from './cardRequestBase';
import { CardDeletionMode, CardDeleteMethod } from 'tessa/cards';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardDeleteRequest extends CardRequestBase implements ICloneable<CardDeleteRequest> {
    constructor(storage?: IStorage);
    static readonly systemDeletionModeKey: string;
    static readonly systemMethodKey: string;
    static readonly keepFileContentKey: string;
    mode: CardDeletionMode;
    method: CardDeleteMethod;
    keepFileContent: boolean;
    clone(): CardDeleteRequest;
}

import { CardResponseBase } from './cardResponseBase';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardDeleteResponse extends CardResponseBase implements ICloneable<CardDeleteResponse> {
    constructor(storage?: IStorage);
    static readonly systemDeletedCardIdKey: string;
    static readonly systemRestoredCardIdKey: string;
    static readonly cardTypeIdKey: string;
    static readonly cardTypeNameKey: string;
    deletedCardId: guid | null;
    restoredCardId: guid | null;
    cardTypeId: guid | null;
    cardTypeName: string | null;
    clean(): void;
    clone(): CardDeleteResponse;
}

import { CardResponseBase } from './cardResponseBase';
import { IStorage } from 'tessa/platform/storage';
import { Card } from 'tessa/cards/card';
export declare class CardRepairResponse extends CardResponseBase {
    constructor(storage?: IStorage);
    static readonly cardKey: string;
    static readonly cardBSONKey: string;
    card: Card;
    cardBSON: string;
    readonly hasCard: boolean;
    readonly hasCardBSON: boolean;
    tryGetCard(): Card | null | undefined;
}

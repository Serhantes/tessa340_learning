import { CardTaskHistoryState } from './cardTaskHistoryState';
import { CardInfoStorageObject } from './cardInfoStorageObject';
import { IStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class CardTaskHistoryGroup extends CardInfoStorageObject {
    constructor(storage: IStorage);
    static readonly rowIdKey: string;
    static readonly parentRowIdKey: string;
    static readonly typeIdKey: string;
    static readonly captionKey: string;
    static readonly systemStateKey: string;
    rowId: guid;
    parentRowId: guid | null;
    typeId: guid;
    caption: string;
    state: CardTaskHistoryState;
    hasChanges(): boolean;
    removeChanges(): boolean;
    isEmpty(): boolean;
}
export declare class CardTaskHistoryGroupFactory implements IStorageValueFactory<CardTaskHistoryGroup> {
    getValue(storage: IStorage): CardTaskHistoryGroup;
    getValueAndStorage(): {
        value: CardTaskHistoryGroup;
        storage: IStorage;
    };
}

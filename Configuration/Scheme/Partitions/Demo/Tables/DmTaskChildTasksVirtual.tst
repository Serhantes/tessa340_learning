﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7a2d63ec-5ea5-42b6-b0d3-d6ea2c1929a8" ID="b91688da-7557-40de-9ccd-86ca1c674e36" Name="DmTaskChildTasksVirtual" Group="Dm" IsVirtual="true" InstanceType="Tasks" ContentType="Collections">
	<Description>Виртуальна секция с информацией о заданиях соисполнителей</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="b91688da-7557-00de-2000-06ca1c674e36" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="b91688da-7557-01de-4000-06ca1c674e36" Name="ID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="b91688da-7557-00de-3100-06ca1c674e36" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="d38887f6-5b4b-47a1-96e2-5621fce4fba9" Name="Performer" Type="Reference(Typified) Not Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="d38887f6-5b4b-00a1-4000-0621fce4fba9" Name="PerformerID" Type="Guid Not Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="6347c926-ded7-4e2a-a241-092f83ec634c" Name="PerformerName" Type="String(128) Not Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="91462d9c-3172-4cc6-b6fe-8c0fbaa15b80" Name="StateName" Type="String(Max) Not Null">
		<Description>Состояние задания</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="79aeffd6-c07d-4a8d-be53-096aa1dd7c2e" Name="Result" Type="String(Max) Not Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="b91688da-7557-00de-5000-06ca1c674e36" Name="pk_DmTaskChildTasksVirtual">
		<SchemeIndexedColumn Column="b91688da-7557-00de-3100-06ca1c674e36" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="b91688da-7557-00de-7000-06ca1c674e36" Name="idx_DmTaskChildTasksVirtual_ID" IsClustered="true">
		<SchemeIndexedColumn Column="b91688da-7557-01de-4000-06ca1c674e36" />
	</SchemeIndex>
</SchemeTable>
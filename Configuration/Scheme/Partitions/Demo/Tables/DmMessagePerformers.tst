﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7a2d63ec-5ea5-42b6-b0d3-d6ea2c1929a8" ID="a9e7f07e-acf7-426e-8174-408b1f81fd0d" Name="DmMessagePerformers" Group="Dm" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="a9e7f07e-acf7-006e-2000-008b1f81fd0d" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="a9e7f07e-acf7-016e-4000-008b1f81fd0d" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="a9e7f07e-acf7-006e-3100-008b1f81fd0d" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="6abe54e4-0dab-4151-a449-a10dc4e41aa5" Name="Performer" Type="Reference(Typified) Not Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="6abe54e4-0dab-0051-4000-010dc4e41aa5" Name="PerformerID" Type="Guid Not Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="d60a4d49-ac23-46b7-b131-9bfe206fc198" Name="PerformerName" Type="String(128) Not Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="a9e7f07e-acf7-006e-5000-008b1f81fd0d" Name="pk_DmMessagePerformers">
		<SchemeIndexedColumn Column="a9e7f07e-acf7-006e-3100-008b1f81fd0d" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="a9e7f07e-acf7-006e-7000-008b1f81fd0d" Name="idx_DmMessagePerformers_ID" IsClustered="true">
		<SchemeIndexedColumn Column="a9e7f07e-acf7-016e-4000-008b1f81fd0d" />
	</SchemeIndex>
</SchemeTable>
﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7ee766f0-f37b-498d-bea3-d96f09e287e7" ID="91108325-63dd-48b6-9aaa-13ce9d6f804b" Name="BranchRegistrators" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="91108325-63dd-00b6-2000-03ce9d6f804b" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="91108325-63dd-01b6-4000-03ce9d6f804b" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="91108325-63dd-00b6-3100-03ce9d6f804b" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="3ed4e246-92e3-4f5b-b104-bbe5929b134d" Name="Role" Type="Reference(Typified) Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="3ed4e246-92e3-005b-4000-0be5929b134d" Name="RoleID" Type="Guid Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="16421a69-444e-4aa7-a827-8b0d52287ab7" Name="RoleName" Type="String(128) Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="91108325-63dd-00b6-5000-03ce9d6f804b" Name="pk_BranchRegistrators">
		<SchemeIndexedColumn Column="91108325-63dd-00b6-3100-03ce9d6f804b" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="91108325-63dd-00b6-7000-03ce9d6f804b" Name="idx_BranchRegistrators_ID" IsClustered="true">
		<SchemeIndexedColumn Column="91108325-63dd-01b6-4000-03ce9d6f804b" />
	</SchemeIndex>
</SchemeTable>
﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7ee766f0-f37b-498d-bea3-d96f09e287e7" ID="8f373089-eb66-411e-ae6c-35354bb0c757" Name="Branches" Group="LC" InstanceType="Cards" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="8f373089-eb66-001e-2000-05354bb0c757" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="8f373089-eb66-011e-4000-05354bb0c757" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="de2d0962-2f14-464f-9866-23e4f66705e0" Name="Name" Type="String(256) Not Null" />
	<SchemePhysicalColumn ID="e9e21108-9a54-4004-a5e5-985beb28da3b" Name="Index" Type="String(256) Null" />
	<SchemeComplexColumn ID="88c44abd-e17e-4b12-92c4-2b1acbe4c6de" Name="HeadUser" Type="Reference(Typified) Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="88c44abd-e17e-0012-4000-0b1acbe4c6de" Name="HeadUserID" Type="Guid Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="3d09f94d-bd61-455f-81af-de13f9537f5f" Name="HeadUserName" Type="String(128) Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="fca92fff-b1b3-4ab4-84d4-792b25a6a333" Name="ParentDep" Type="Reference(Typified) Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="fca92fff-b1b3-00b4-4000-092b25a6a333" Name="ParentDepID" Type="Guid Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="cf92f4ba-19d3-4775-9c4e-f6d11dfdd26e" Name="ParentDepName" Type="String(128) Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="8f373089-eb66-001e-5000-05354bb0c757" Name="pk_Branches" IsClustered="true">
		<SchemeIndexedColumn Column="8f373089-eb66-011e-4000-05354bb0c757" />
	</SchemePrimaryKey>
</SchemeTable>